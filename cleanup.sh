
# array of items, relative to ATLAS_LOCAL_ROOT_BASE, that will be removed
mt3sw_CleanupAr=(
    "i686" 
    "rucio"
    "gLite"
    "Athena"
    "etc/grid-security"
    "etc/vomses/atlas-voms-atlas-auth.app.cern.ch"
    "etc/vomses/atlas-lcg-voms2.cern.ch"
    "etc/vomses/atlas-voms2.cern.ch"
    "x86_64/PandaClient/currentJedi"
    "x86_64/root/%1"
    "x86_64-MacOS/root/%1"
    "x86_64/DQ2Client"
    "x86_64-MacOS/DQ2Client"
    "x86_64/gLite"
    "x86_64-MacOS/gLite"
    "x86_64/rucio"
    "x86_64-MacOS/rucio"
    "x86_64/root/current-proof"
    "x86_64/Atlantis/dev"
    "x86_64/root/.mapdir"
    "x86_64-MacOS/root/.mapdir"
    "x86_64-MacOS/.lcgarea"
    "x86_64-MacOS/.alrb/swList"
    "x86_64/.alrb/swList"
    "x86_64/xrdcache"
    "x86_64/PandaClient/0.5.80"
    "x86_64/PandaClient/0.5.80"
    "x86_64/inotifytools"
    "x86_64/python/pilotTest"
    "x86_64/AGIS"
    "x86_64/FAXTools"
    "x86_64/Ganga"
    "x86_64/PoD"
    "aarch64-Linux/AGIS"
    "logDir/containers.unpackedRepo.txt"
    "compat"
    "x86_64-MacOS/EIClient"
    "x86_64-MacOS/AtlasSetup"
    "x86_64-MacOS/rcSetup"
    "x86_64-MacOS/Atlantis"
    "x86_64-MacOS/AGIS"
    "x86_64-MacOS/FAXTools"
    "x86_64-MacOS/Ganga"
    "x86_64-MacOS/PoD"
)

if [ -e $ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/certificates ]; then
    chmod 755 $ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/certificates
fi

# fix for older emi versions
alrb_oldPwd=`pwd`
alrb_tmpAr=()
if [ -d $ATLAS_LOCAL_ROOT_BASE/x86_64/emi ]; then
    alrb_tmpAr=( ${alrb_tmpAr[@]}
	`\find $ATLAS_LOCAL_ROOT_BASE/x86_64/emi -mindepth 4 -maxdepth 4 -name pathPriority  -type d`
    )
fi
if [ -d $ATLAS_LOCAL_ROOT_BASE/aarch64-Linux/emi ]; then
    alrb_tmpAr=( ${alrb_tmpAr[@]}
	`\find $ATLAS_LOCAL_ROOT_BASE/aarch64-Linux/emi -mindepth 4 -maxdepth 4 -name pathPriority  -type d`
    )
fi
for alrb_item in ${alrb_tmpAr[@]}; do
    if [ ! -e $alrb_item/voms-proxy-init ]; then
	cd $alrb_item
	if [[ ! -e voms-proxy-init ]] && [[ -e ../voms-proxy-init3 ]] ; then
	    ln -s ../voms-proxy-init3 voms-proxy-init
	fi
	if [[ ! -e voms-proxy-info ]] && [[ -e ../voms-proxy-info3 ]]; then
	    ln -s ../voms-proxy-info3 voms-proxy-info
	fi
	if [[ ! -e voms-proxy-destroy ]] && [[ -e ../voms-proxy-destroy3 ]]; then
	    ln -s ../voms-proxy-destroy3 voms-proxy-destroy
	fi	
    fi
done
cd $alrb_oldPwd

# update containerInformation files
alrb_oldPwd=`pwd`
mkdir -p $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo
\cp -r $mt3sw_configDir/containerInfo/* $ATLAS_LOCAL_ROOT_BASE/etc/containerInfo/
$ATLAS_LOCAL_ROOT_BASE/container/createConvertToAliasSedFile.sh
cd $alrb_oldPwd

# update other files
\cp $mt3sw_configDir/ADCPilotTesting.sh $ATLAS_LOCAL_ROOT_BASE/etc/

# update voms files

mkdir -p $ATLAS_LOCAL_ROOT_BASE/etc/vomses
\cp -r $mt3sw_configDir/voms/etc/vomses/* $ATLAS_LOCAL_ROOT_BASE/etc/vomses/

mkdir -p $ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/vomsdir/atlas
\cp -r $mt3sw_configDir/voms/etc/grid-security/vomsdir/atlas/* $ATLAS_LOCAL_ROOT_BASE/etc/grid-security-emi/vomsdir/atlas/
