# These are the ATLAS projects and registries to use for finding:
# format is
#  project_name&prefix_alias
# in the search order where project_name is all lowercase (like in registries)

alrb_relProjAr=(
    "athsimulation&CH/A0"
    "analysisbase&CG/AA"
    "athanalysis&CG/AA"
    "athsimulation&CG/AA"
    "athena&DR/A0"
)
