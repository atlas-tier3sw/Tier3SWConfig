# These are versions that should be used for ADC Testing of ALRB tools
#  eg http://hammercloud.cern.ch/hc/app/atlas/#ALRB

export ALRB_asetupVersion=testing

export ALRB_emiVersion=testing

export ALRB_davixVersion=testing

export ALRB_rucioVersion=testing

export ALRB_xrootdVersion=testing

export ALRB_cvmfsSingularityVersion=testing

# do not overwrite Apptainer versions if specifically defined
# eg K8S depends on host OS and if centos7 must run apptainer 1.2.2
if [ -z $ALRB_cvmfsApptainerVersion ]; then
  export ALRB_cvmfsApptainerVersion=testing
fi

export ALRB_logstashVersion=testing

export ALRB_psutilVersion=testing

export ALRB_pythonVersion=pilot-testing

export ALRB_pacparserVersion=testing

export ALRB_cpuflagsVersion=testing

export ALRB_curlVersion=testing

export ALRB_prmonVersion=testing

export ALRB_stompVersion=testing
