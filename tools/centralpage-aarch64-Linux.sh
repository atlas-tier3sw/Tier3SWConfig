# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="0.0.5"
alrb_InstallPrevious="0.0.3"
alrb_InstallTesting="0.0.5"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="0.0.3"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/centralpage/central-page_cli_v${alrb_InstallVersion}.tar.gz"
    alrb_ToolInstallTag="1727805113"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.0.4"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/centralpage/central-page-cli_v${alrb_InstallVersion}.tar.gz"
    alrb_InstallAction="remove"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.0.5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/centralpage/central-page-cli_v${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)
