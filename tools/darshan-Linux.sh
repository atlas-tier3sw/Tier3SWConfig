# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="3.4.2-fix1-x86_64-centos7"
alrb_InstallTesting="3.4.2-fix1-x86_64-centos7"
alrb_InstallPrevious="3.4.2-x86_64-centos7"

alrb_InstallDefaultSL9="3.4.2-fix1-x86_64-centos9"
alrb_InstallTestingSL9="3.4.2-fix1-x86_64-centos9"
alrb_InstallPreviousSL9="3.4.2-x86_64-centos9"

alrb_InstallDefaultSL8="3.4.2-fix1-x86_64-centos8"
alrb_InstallTestingSL8="3.4.2-fix1-x86_64-centos8"
alrb_InstallPreviousSL8="3.4.2-x86_64-centos8"

alrb_InstallDefaultSL7="$alrb_InstallDefault"
alrb_InstallTestingSL7="$alrb_InstallTesting"
alrb_InstallPreviousSL7="$alrb_InstallPrevious"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="3.4.2-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/darshan/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.4.2-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/darshan/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.4.2-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/darshan/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.4.2-fix1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/darshan/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.4.2-fix1-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/darshan/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.4.2-fix1-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/darshan/$alrb_InstallVersion.tar.gz"
    alrb_InstallAlternateNames=( "3.4.2-fix1-x86_64-el9" )
    alrb_fn_installAction
)

