# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="3.9.21-aarch64-el9"
alrb_InstallRecommended="3.9.21-aarch64-el9"

alrb_InstallPilot="3.11.10-aarch64-el9"
alrb_InstallPilotTesting="3.12.9-aarch64-el9"

alrb_InstallPilotSL9="$alrb_InstallPilot"
alrb_InstallPilotTestingSL9="$alrb_InstallPilotTesting"
alrb_InstallRecommendedSL9="$alrb_InstallRecommended"

alrb_InstallPilotSL8="3.11.10-aarch64-centos8"
alrb_InstallPilotTestingSL8="3.12.9-aarch64-centos8"
alrb_InstallRecommendedSL8="3.9.21-aarch64-centos8"

# frozen / obsolete
alrb_InstallPilotSL7="3.8.13-fix1-aarch64-centos7"
alrb_InstallPilotTestingSL7="3.8.13-fix1-aarch64-centos7"
alrb_InstallRecommendedSL7="3.8.13-fix1-aarch64-centos7"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="3.7.12-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.8.12-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.9-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.7.13-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction="archive"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.8.13-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction="archive"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.11-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAlternateNames=( "centos7-3.9" )
#    alrb_InstallAction="archive"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.8.13-fix1-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/3.8.13-aarch64-centos7.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.7.13-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.8.13-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.11-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.13-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.9" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.13-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.14-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.9" "recommended-SL9" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.10.7-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.3-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.4-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.11" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.18-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos7-2.7" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.18-fix1-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAlternateNames=( "centos7-2.7" )
    alrb_ToolInstallTag="1695339945"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.18-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.9" )
    alrb_ToolInstallTag="1700687433"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.18-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.9" "el9-3.9" )
    alrb_ToolInstallTag="1695339945"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.19-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.9" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.19-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.9" "el9-3.9" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.20-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.20-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.9" "el9-3.9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.21-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAlternateNames=( "centos8-3.9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.21-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAlternateNames=( "centos9-3.9" "el9-3.9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.5-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.11" "el9-3.11" )
    alrb_ToolInstallTag="1695339945"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.6-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.11" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.7-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.11" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.7-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.11" "el9-3.11" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.9-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.11" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.9-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.11" "el9-3.11" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.10-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAlternateNames=( "centos8-3.11" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.10-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAlternateNames=( "centos9-3.11" "el9-3.11" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.4-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.4-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="3.12.5-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.12" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.5-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.12" "el9-3.12" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.6-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.12" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.6-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.12" "el9-3.12" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.7-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    #    alrb_InstallAlternateNames=( "centos8-3.12" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.7-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    #    alrb_InstallAlternateNames=( "centos9-3.12" "el9-3.12" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.8-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.12" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.8-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.12" "el9-3.12" )
    alrb_fn_installAction
)


(
    alrb_InstallVersion="3.12.9-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAlternateNames=( "centos8-3.12" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.9-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/$alrb_InstallVersion.tgz"
    alrb_InstallAlternateNames=( "centos9-3.12" "el9-3.12" )
    alrb_fn_installAction
)
