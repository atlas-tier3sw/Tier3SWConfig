# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="3.0.0-x86_64-el9"
alrb_InstallTesting="3.0.0-x86_64-el9"
alrb_InstallPrevious="2.5.0-x86_64-centos9"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL8="3.0.0-x86_64-centos8"
alrb_InstallTestingSL8="3.0.0-x86_64-centos8"
alrb_InstallPreviousSL8="2.5.0-fix1-x86_64-centos8"

# this is frozen (needs OpenSSL 1.1.1 for newer versions"
alrb_InstallDefaultSL7="2.3.0"
alrb_InstallPreviousSL7="2.3.0"
alrb_InstallTestingSL7="2.3.0"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="2.3.0-beta"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/logstash/logstash-2.3.0.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.3.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/logstash/2.3.0-x86_64-centos7.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.5.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/logstash/$alrb_InstallVersion.tar.gz"
    alrb_ToolInstallTag="1690410319"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.5.0-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/logstash/$alrb_InstallVersion.tar.gz"
    alrb_ToolInstallTag="1690410319"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.5.0-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/logstash/$alrb_InstallVersion.tar.gz"
    alrb_ToolInstallTag="1690410319"
    alrb_InstallAlternateNames=( "2.5.0-x86_64-el9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.5.0-fix1-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/logstash/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.0.0-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/logstash/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.0.0-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/logstash/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)
