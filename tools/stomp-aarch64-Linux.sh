# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="7.0.0-fix2-aarch64-el9"
alrb_InstallPrevious="7.0.0-aarch64-centos9"
alrb_InstallTesting="7.0.0-fix2-aarch64-el9"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"
alrb_InstallTestingSL9="$alrb_InstallTesting"

alrb_InstallDefaultSL8="7.0.0-fix2-aarch64-centos8"
alrb_InstallPreviousSL8="7.0.0-fix1-aarch64-centos8"
alrb_InstallTestingSL8="7.0.0-fix2-aarch64-centos8"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="7.0.0-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/stomp/${alrb_InstallVersion}.tar.gz"
    alrb_ToolInstallTag="1690406964"
    alrb_InstallAlternateNames=( "7.0.0-aarch64-el9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="7.0.0-fix1-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/stomp/${alrb_InstallVersion}.tar.gz"
    alrb_ToolInstallTag="1700885975"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="7.0.0-fix2-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/stomp/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="7.0.0-fix2-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/stomp/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)
