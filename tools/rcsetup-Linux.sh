# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

# obsolete
alrb_installActionIgnore="YES"

alrb_InstallDefault="00-04-18"

alrb_InstallTesting="$alrb_InstallDefault"

alrb_InstallPlatform="any"

(
    alrb_InstallVersion="00-04-18"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/rcSetup/rcSetup-$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

