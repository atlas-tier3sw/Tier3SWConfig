# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="2.14-aarch64-el9-gcc13"
alrb_InstallTesting="2.14-aarch64-el9-gcc13"
alrb_InstallPrevious="2.12-aarch64-el9-gcc11"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL7="2.14-aarch64-centos7-gcc11"
alrb_InstallPreviousSL7="2.12-aarch64-centos7-gcc11"
alrb_InstallTestingSL7="2.14-aarch64-centos7-gcc11"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="2.11-aarch64-centos7-gcc11"
    if [ `uname -m` = "aarch64" ]; then
        alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.11.1.tar.gz"
    	alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc11_v1.sh"
    else
        alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/$alrb_InstallVersion.tgz"	
    fi
    alrb_ToolInstallTag="1692910594"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.11-aarch64-el9-gcc11"
    if [ `uname -m` = "aarch64" ]; then
	alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.11.1.tar.gz"
	alrb_PostInstallUseContainer="el9|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc11_v1.sh"
    else
        alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/$alrb_InstallVersion.tgz"
    fi
    alrb_ToolInstallTag="1692910594"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.11-aarch64-el9-gcc13"
    if [ `uname -m` = "aarch64" ]; then
	alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.11.1.tar.gz"
	alrb_PostInstallUseContainer="el9|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc13_v1.sh"
    else
        alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/$alrb_InstallVersion.tgz"
    fi
    alrb_ToolInstallTag="1692910594"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.12-aarch64-centos7-gcc11"
    if [ `uname -m` = "aarch64" ]; then
        alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.12.0.tar.gz"
    	alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc11_v1.sh"
    else
        alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/$alrb_InstallVersion.tgz"	
    fi
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.12-aarch64-el9-gcc11"
    if [ `uname -m` = "aarch64" ]; then
	alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.12.0.tar.gz"
	alrb_PostInstallUseContainer="el9|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc11_v1.sh"
    else
        alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/$alrb_InstallVersion.tgz"
    fi
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.12-aarch64-el9-gcc13"
    if [ `uname -m` = "aarch64" ]; then
	alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.12.0.tar.gz"
	alrb_PostInstallUseContainer="el9|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc13_v1.sh"
    else
        alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/$alrb_InstallVersion.tgz"
    fi
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.14-aarch64-centos7-gcc11"
    if [ `uname -m` = "aarch64" ]; then
        alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.14.1.tar.gz"
    	alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc11_v1.sh"
    else
        alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/$alrb_InstallVersion.tgz"	
    fi
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.14-aarch64-el9-gcc11"
    if [ `uname -m` = "aarch64" ]; then
	alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.14.1.tar.gz"
	alrb_PostInstallUseContainer="el9|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc11_v1.sh"
    else
        alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/$alrb_InstallVersion.tgz"
    fi
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.14-aarch64-el9-gcc13"
    if [ `uname -m` = "aarch64" ]; then
	alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.14.1.tar.gz"
	alrb_PostInstallUseContainer="el9|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc13_v1.sh"
    else
        alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/$alrb_InstallVersion.tgz"
    fi
    alrb_fn_installAction
)


