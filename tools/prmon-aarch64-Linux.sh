# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="3.1.0"

alrb_InstallPrevious="3.0.0"

alrb_InstallTesting="3.1.0"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="3.0.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/prmon/prmon_${alrb_InstallVersion}_aarch64-static-gnu85-opt.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.1.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/prmon/prmon_${alrb_InstallVersion}_aarch64-static-gnu114-opt.tar.gz"
    alrb_fn_installAction
)

