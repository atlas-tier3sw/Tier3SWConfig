# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

# inportant note for above ... rucio 1.20.0 and newer only for CentOS7
#  default is always latest unless overridden below
alrb_InstallDefault="35.6.0"
alrb_InstallPrevious="34.1.0"
alrb_InstallTesting="35.6.0"

alrb_InstallDefaultSL7="$alrb_InstallDefault"
alrb_InstallTestingSL7="$alrb_InstallTesting"
alrb_InstallPreviousSL7="$alrb_InstallPrevious"

# frozen
alrb_InstallDefaultSL6="1.19.8"  
alrb_InstallTestingSL6="1.19.8" 
alrb_InstallPreviousSL6="1.19.8"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="1.19.8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/rucio-clients/rucio-clients-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.28.4"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/rucio-clients/rucio-clients-${alrb_InstallVersion}.tar.gz"
    alrb_InstallAction='archive'
    #    alrb_InstallAlternateNames=( "SL7Python2" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.28.4-fix1"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/rucio-clients/rucio-clients-${alrb_InstallVersion}.tar.gz"
    alrb_InstallAlternateNames=( "SL7Python2" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="32.1.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/rucio-clients/rucio-clients-${alrb_InstallVersion}.tar.gz"
    alrb_ToolInstallTag="1687967092"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="32.5.1"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/rucio-clients/rucio-clients-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="33.3.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/rucio-clients/rucio-clients-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="34.1.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/rucio-clients/rucio-clients-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="35.6.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/rucio-clients/rucio-clients-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

