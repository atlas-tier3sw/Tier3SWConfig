# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="3.1.0"

alrb_InstallPrevious="3.0.0"

alrb_InstallTesting="3.1.0"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="2.0.1"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/prmon/prmon_2.0.1_x86_64-static-gnu93-opt.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.0.2"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/prmon/prmon_${alrb_InstallVersion}_x86_64-static-gnu93-opt.tar.gz"
    alrb_ToolInstallTag="1595608129"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.1.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/prmon/prmon_${alrb_InstallVersion}_x86_64-static-gnu93-opt.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.1.1"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/prmon/prmon_${alrb_InstallVersion}_x86_64-static-gnu93-opt.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.2.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/prmon/prmon_${alrb_InstallVersion}_x86_64-static-gnu93-opt.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.0.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/prmon/prmon_${alrb_InstallVersion}_x86_64-static-gnu85-opt.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.1.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/prmon/prmon_${alrb_InstallVersion}_x86_64-static-gnu113-opt.tar.gz"
    alrb_fn_installAction
)

