# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallRecommended="6.34.04-aarch64-el9-gcc13-opt"
alrb_InstallRecommendedSL9="$alrb_InstallRecommended"

alrb_InstallPilotSL9="6.32.06-aarch64-el9-gcc13-opt"
alrb_InstallPilotTestingSL9="6.34.04-aarch64-el9-gcc13-opt"

# frozen
alrb_InstallRecommendedSL7="6.30.02-aarch64-centos7-gcc11-opt"
# need this for testing with root, should be able to run 1 gen older OS version
alrb_InstallRecommendedSL8="$alrb_InstallRecommendedSL7"

alrb_InstallPilotSL7="6.30.02-aarch64-centos7-gcc11-opt"
alrb_InstallPilotTestingSL7="6.30.02-aarch64-centos7-gcc11-opt"

alrb_InstallPilotSL8="$alrb_InstallPilotSL7"
alrb_InstallPilotTestingSL8="$alrb_InstallPilotTestingSL7"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="6.18.04-dev-aarch64-centos7-gcc8-opt"
    alrb_InstallViews="devARM latest/aarch64-centos7-gcc8-opt"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.24.06-aarch64-centos7-gcc8-opt"
    alrb_InstallLcgEnv="-p LCG_101arm aarch64-centos7-gcc8-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.26.04-aarch64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_102 aarch64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.26.08-aarch64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_102b_ATLAS_2 aarch64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.00-aarch64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_103 aarch64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.00-aarch64-centos9-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_103 aarch64-centos9-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.04-aarch64-el9-gcc12-opt"
    alrb_InstallLcgEnv="-p LCG_104 aarch64-el9-gcc12-opt ROOT"
    alrb_ToolInstallTag="1691515842"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.04-aarch64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_104 aarch64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.04-aarch64-el9-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_104 aarch64-el9-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.04-aarch64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_104 aarch64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.06-aarch64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_104a aarch64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.06-aarch64-el9-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_104a aarch64-el9-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.06-aarch64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_104a aarch64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.08-aarch64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_104b_ATLAS_2 aarch64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.08-aarch64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_104b_ATLAS_2 aarch64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.10-aarch64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_104c_ATLAS_2 aarch64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.10-aarch64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_104c_ATLAS_2 aarch64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.30.02-aarch64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_105 aarch64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.30.02-aarch64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_105 aarch64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.32.02-aarch64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_106 aarch64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.32.06-aarch64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_106a_ATLAS_2 aarch64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.32.08-aarch64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_106b_ATLAS_2 aarch64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.34.04-aarch64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_107a_ATLAS_2 aarch64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

