# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="1.5.85"
alrb_InstallTesting="$alrb_InstallDefault"
alrb_InstallPrevious="1.5.83"

alrb_InstallPlatform="aarch64-Linux"

alrb_InstallTarballOptions="--strip-components=1"

alrb_InstallRelocateFilesAr=( "etc/panda/panda_setup.csh" "etc/panda/panda_setup.sh" )


(
    alrb_InstallVersion="1.5.68"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.70"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

# do not delete this version without consulting
#  Alexander Lory <a.lory@cern.ch> (need it for HC)
(
    alrb_InstallVersion="1.5.71"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.73"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.74"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.75"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.76"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.77"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.78"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.79"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.80"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.81"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_InstallAction="remove"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.82"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.83"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.85"
    alrb_InstallTarballDownload="https://github.com/PanDAWMS/panda-client/archive/refs/tags/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)
