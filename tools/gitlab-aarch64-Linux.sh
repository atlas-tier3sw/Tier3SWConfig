# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="3.9.0-aarch64-centos9"
alrb_InstallTesting="3.9.0-aarch64-centos9"
alrb_InstallPrevious="3.8.1-aarch64-centos9"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL7="3.9.0-aarch64-centos7"
alrb_InstallTestingSL7="3.9.0-aarch64-centos7"
alrb_InstallPreviousSL7="3.8.1-aarch64-centos7"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="3.8.1-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/gitlab/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.8.1-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/gitlab/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)


(
    alrb_InstallVersion="3.9.0-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/gitlab/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.0-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/gitlab/${alrb_InstallVersion}.tar.gz"
    alrb_InstallAlternateNames=( "3.9.0-aarch64-el9" )
    alrb_fn_installAction
)

