# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallPlatform="x86_64"

alrb_InstallAction='archive'

(
    alrb_InstallVersion="1.6.17-x86_64_slc6_gcc62_opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/png/png-1.6.17-1a97c_x86_64_slc6_gcc62_opt.tgz"
    alrb_fn_installAction
)

