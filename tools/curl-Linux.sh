# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="8.7.1-x86_64-el9"
alrb_InstallTesting="8.7.1-x86_64-el9"
alrb_InstallPrevious="8.7.1-x86_64-el9"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL8="8.7.1-x86_64-centos8"
alrb_InstallTestingSL8="8.7.1-x86_64-centos8"
alrb_InstallPreviousSL8="8.7.1-x86_64-centos8"

alrb_InstallDefaultSL7="7.29.0-x86_64-centos7"
alrb_InstallPreviousSL7="7.29.0-x86_64-centos7"
alrb_InstallTestingSL7="7.29.0-x86_64-centos7"

alrb_InstallPlatform="x86_64"

(
    # this is a dummy version to default to the OS installed one
    alrb_InstallVersion="7.29.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/curl/doNothing.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="8.7.1-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/curl/${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="8.7.1-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/curl/${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

