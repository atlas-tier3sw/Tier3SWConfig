# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="0.20.2"
alrb_InstallPrevious="0.19.0"

alrb_InstallPlatform="arm64-MacOS"

(
    alrb_InstallVersion="0.19.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/crane/go-containerregistry-v${alrb_InstallVersion}-Darwin-arm64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.20.2"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/crane/go-containerregistry-v${alrb_InstallVersion}-Darwin-arm64.tar.gz"
    alrb_fn_installAction
)
