# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="7.3.0"
alrb_InstallTesting="7.3.0"
alrb_InstallPrevious="7.2.0"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL7="$alrb_InstallDefault"
alrb_InstallTestingSL7="$alrb_InstallTesting"
alrb_InstallPreviousSL7="$alrb_InstallPrevious"

alrb_InstallDefaultSL6="$alrb_InstallDefault"
alrb_InstallTestingSL6="$alrb_InstallTesting"
alrb_InstallPreviousSL6="$alrb_InstallPrevious"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="190904"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-190904.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="200210"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-200210.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="200305"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-200305.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="200414"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="200610"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="200611"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="200615"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="200907"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="210903"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="220916"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="230403"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="230421"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="7.0.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="7.1.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="7.2.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="7.3.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

