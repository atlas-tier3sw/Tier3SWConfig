# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallRecommended="6.34.04-x86_64-el9-gcc13-opt"
alrb_InstallRecommendedSL9="$alrb_InstallRecommended"

alrb_InstallPilotSL9="6.32.06-x86_64-el9-gcc13-opt"
alrb_InstallPilotTestingSL9="6.34.04-x86_64-el9-gcc13-opt"

# frozen / obsolete
alrb_InstallRecommendedSL6="6.14.04-x86_64-slc6-gcc62-opt"
alrb_InstallRecommendedSL7="6.30.02-x86_64-centos7-gcc11-opt"
alrb_InstallRecommendedSL8="6.26.04-x86_64-centos8-gcc11-opt"

alrb_InstallPilotSL7="6.30.02-x86_64-centos7-gcc11-opt"
alrb_InstallPilotTestingSL7="6.30.02-x86_64-centos7-gcc11-opt"

alrb_InstallPilotSL8="$alrb_InstallPilotSL7"
alrb_InstallPilotTestingSL8="$alrb_InstallPilotTestingSL7"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="5.34.10-i686-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.10-x86_64-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.10-x86_64-slc6-gcc4.7"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.11-i686-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.11-x86_64-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.11-x86_64-slc6-gcc4.7"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.12-i686-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.12-x86_64-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.12-x86_64-slc6-gcc4.7"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.13-i686-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.13-x86_64-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.13-x86_64-slc6-gcc4.7"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.14-i686-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.14-x86_64-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.14-x86_64-slc6-gcc4.7"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.14-x86_64-slc6-gcc4.8"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.17-i686-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.17-x86_64-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.17-x86_64-slc6-gcc4.7"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.17-x86_64-slc6-gcc4.8"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.18-i686-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.18-x86_64-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.18-x86_64-slc6-gcc4.7"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.18-x86_64-slc6-gcc4.8"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.19-i686-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.19-x86_64-slc5-gcc4.3"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.19-x86_64-slc6-gcc4.7"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.19-x86_64-slc6-gcc4.8"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.21-x86_64-slc6-gcc47-opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.21-x86_64-slc6-gcc48-opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.22-x86_64-slc6-gcc48-opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.24-x86_64-slc6-gcc48-opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.34.25-x86_64-slc6-gcc48-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-5.34.25_x86_64_slc6_gcc48_opt.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

# 5.34.32 SLC6 HiggsComb special version
(
    alrb_InstallVersion="5.34.32-HiggsComb-x86_64-slc6-gcc48-opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

# 5.34.32 SLC6 HiggsComb special version
(
    alrb_InstallVersion="5.34.32-HiggsComb-p1-x86_64-slc6-gcc48-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-5.34.32-HiggsComb-p1-x86_64-slc6-gcc48-opt.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.00.00-x86_64-slc6-gcc48-opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.00.01-x86_64-slc6-gcc48-opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.00.02-x86_64-slc6-gcc48-opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.02.00-x86_64-slc6-gcc48-opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.02.01-x86_64-slc6-gcc48-opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.02.02-x86_64-slc6-gcc48-opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.02.03-x86_64-slc6-gcc48-opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.02.04-x86_64-slc6-gcc48-opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.02.05-x86_64-slc6-gcc48-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.02.05_x86_64_slc6_gcc48_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.02.10-x86_64-slc6-gcc48-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.02.10.p1_x86_64_slc6_gcc48_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.02.12-x86_64-slc6-gcc48-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.02.12_x86_64_slc6_gcc48_opt.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.04.02-x86_64-slc6-gcc48-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.04.02_x86_64_slc6_gcc48_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.04.06-x86_64-slc6-gcc48-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.04.06_x86_64_slc6_gcc48_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.04.10-x86_64-slc6-gcc48-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.04.10_x86_64_slc6_gcc48_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.04.12-x86_64-slc6-gcc49-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.04.12_x86_64_slc6_gcc49_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.04.14-x86_64-slc6-gcc49-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.04.14_x86_64_slc6_gcc49_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.04.16-x86_64-slc6-gcc49-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.04.16_x86_64_slc6_gcc49_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

# 6.04.16 SLC6 HiggsComb special version
(
    alrb_InstallVersion="6.04.16-HiggsComb-x86_64-slc6-gcc49-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.04.16-HiggsComb-x86_64-slc6-gcc49-opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.04.18-x86_64-slc6-gcc49-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.04.18_x86_64_slc6_gcc49_opt.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.06.02-x86_64-slc6-gcc48-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.06.02_x86_64_slc6_gcc48_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.06.02-x86_64-slc6-gcc49-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.06.02_x86_64_slc6_gcc49_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.06.06-x86_64-slc6-gcc49-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.06.06_x86_64_slc6_gcc49_opt.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.08.00-x86_64-slc6-gcc49-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.08.00_x86_64_slc6_gcc49_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.08.02-x86_64-slc6-gcc49-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.08.02_x86_64_slc6_gcc49_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.08.02-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.08.02_x86_64_slc6_gcc62_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.08.06-x86_64-slc6-gcc49-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.08.06_x86_64_slc6_gcc49_opt.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.08.06-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.08.06_x86_64_slc6_gcc62_opt.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

# 6.08.06 SLC6 HiggsComb special version
(
    alrb_InstallVersion="6.08.06-HiggsComb-x86_64-slc6-gcc49-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.08.06-HiggsComb-x86_64-slc6-gcc49-opt.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.10.02-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.10.02_x86_64_slc6_gcc62_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.10.04-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.10.04_x86_64_slc6_gcc62_opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.10.06-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/root-6.10.06_x86_64_slc6_gcc62_opt.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.12.04-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/ROOT-cbbb8_6.12.04_x86_64_slc6_gcc62_opt-1.0.0-92.noarch.rpm.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.12.06-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/ROOT-0f687_6.12.06_x86_64_slc6_gcc62_opt-1.0.0-93.noarch.rpm.tgz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.14.04-x86_64-slc6-gcc62-opt"
    alrb_InstallLcgEnv="-p LCG_94 x86_64-slc6-gcc62-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.14.08-x86_64-slc6-gcc62-opt"
    alrb_InstallLcgEnv="-p LCG_94a x86_64-slc6-gcc62-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.14.08-x86_64-centos7-gcc8-opt"
    alrb_InstallLcgEnv="-p LCG_94a x86_64-centos7-gcc8-opt ROOT"
    alrb_fn_installAction
)


# 6.04.16 Patched SLC6 HiggsComb special version
(
    alrb_InstallVersion="6.04.16-HiggsComb-p1-x86_64-slc6-gcc49-opt"
#    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/6.04.16-HiggsComb-p1-x86_64-slc6-gcc49-opt.tar.gz"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/root/6.04.16-HiggsComb-p1a-x86_64-slc6-gcc49-opt.tar.gz"
    alrb_ToolInstallTag="1549674860"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="6.16.00-x86_64-slc6-gcc62-opt"
    alrb_InstallLcgEnv="-p LCG_95 x86_64-slc6-gcc62-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.16.00-x86_64-centos7-gcc8-opt"
    alrb_InstallLcgEnv="-p LCG_95 x86_64-centos7-gcc8-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.18.00-x86_64-slc6-gcc8-opt"
    alrb_InstallLcgEnv="-p LCG_96 x86_64-slc6-gcc8-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.18.00-x86_64-centos7-gcc8-opt"
    alrb_InstallLcgEnv="-p LCG_96 x86_64-centos7-gcc8-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.18.04-x86_64-centos7-gcc8-opt"
    alrb_InstallLcgEnv="-p LCG_96b x86_64-centos7-gcc8-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.20.02-x86_64-centos7-gcc8-opt"
    alrb_InstallLcgEnv="-p LCG_97_ATLAS_1 x86_64-centos7-gcc8-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.20.06-x86_64-centos7-gcc8-opt"
    alrb_InstallLcgEnv="-p LCG_97a x86_64-centos7-gcc8-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.22.00-x86_64-centos7-gcc8-opt"
    alrb_InstallLcgEnv="-p LCG_98 x86_64-centos7-gcc8-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.22.00-python3-x86_64-centos7-gcc8-opt"
    alrb_InstallLcgEnv="-p LCG_98python3_ATLAS_1 x86_64-centos7-gcc8-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.22.06-x86_64-centos7-gcc8-opt"
    alrb_InstallLcgEnv="-p LCG_99 x86_64-centos7-gcc8-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.24.06-x86_64-centos7-gcc8-opt"
    alrb_InstallLcgEnv="-p LCG_101 x86_64-centos7-gcc8-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.26.04-x86_64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_102 x86_64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.26.04-x86_64-centos8-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_102 x86_64-centos8-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.26.04-x86_64-centos9-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_102 x86_64-centos9-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.26.08-x86_64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_102b_ATLAS_2 x86_64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.26.08-x86_64-centos9-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_102b_ATLAS_2 x86_64-centos9-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.00-x86_64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_103 x86_64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.00-x86_64-centos9-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_103 x86_64-centos9-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.04-x86_64-centos7-gcc12-opt"
    alrb_InstallLcgEnv="-p LCG_104 x86_64-centos7-gcc12-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.04-x86_64-el9-gcc12-opt"
    alrb_InstallLcgEnv="-p LCG_104 x86_64-el9-gcc12-opt ROOT"
    alrb_ToolInstallTag="1691515842"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.04-x86_64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_104 x86_64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.04-x86_64-el9-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_104 x86_64-el9-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.04-x86_64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_104 x86_64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.06-x86_64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_104a x86_64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.06-x86_64-el9-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_104a x86_64-el9-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.06-x86_64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_104a x86_64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)


(
    alrb_InstallVersion="6.28.08-x86_64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_104b_ATLAS_2 x86_64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)


(
    alrb_InstallVersion="6.28.08-x86_64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_104b_ATLAS_2 x86_64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.10-x86_64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_104c_ATLAS_2 x86_64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.28.10-x86_64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_104c_ATLAS_2 x86_64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.30.02-x86_64-centos7-gcc11-opt"
    alrb_InstallLcgEnv="-p LCG_105 x86_64-centos7-gcc11-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.30.02-x86_64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_105 x86_64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.32.02-x86_64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_106 x86_64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.32.06-x86_64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_106a_ATLAS_2 x86_64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.32.08-x86_64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_106b_ATLAS_2 x86_64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.34.04-x86_64-el9-gcc13-opt"
    alrb_InstallLcgEnv="-p LCG_107a_ATLAS_2 x86_64-el9-gcc13-opt ROOT"
    alrb_fn_installAction
)


