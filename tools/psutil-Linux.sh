# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="6.0.0-x86_64-el9"
alrb_InstallTesting="6.0.0-x86_64-el9"
alrb_InstallPrevious="5.9.5-x86_64-centos9"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL8="6.0.0-x86_64-centos8"
alrb_InstallTestingSL8="6.0.0-x86_64-centos8"
alrb_InstallPreviousSL8="5.9.5-fix1-x86_64-centos8"

alrb_InstallDefaultSL7="6.0.0-x86_64-centos7"
alrb_InstallPreviousSL7="5.9.5-x86_64-centos7"
alrb_InstallTestingSL7="6.0.0-x86_64-centos7"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="5.9.5-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/psutil/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.9.5-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/psutil/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.9.5-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/psutil/$alrb_InstallVersion.tar.gz"
    alrb_InstallAlternateNames=( "5.9.5-x86_64-el9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.9.5-fix1-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/psutil/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.0.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/psutil/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.0.0-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/psutil/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="6.0.0-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/psutil/$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

