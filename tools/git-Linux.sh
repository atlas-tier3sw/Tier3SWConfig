# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="2.41.0-x86_64-centos7"
alrb_InstallTesting="2.41.0-x86_64-centos7"
alrb_InstallPrevious="2.39.0-x86_64-centos7"

alrb_InstallDefaultSL7="$alrb_InstallDefault"
alrb_InstallTestingSL7="$alrb_InstallTesting"
alrb_InstallPreviousSL7="$alrb_InstallPrevious"

# frozen
alrb_InstallDefaultSL6="2.11.1-x86_64-slc6"
alrb_InstallTestingSL6="2.11.1-x86_64-slc6"
alrb_InstallPreviousSL6="2.11.1-x86_64-slc6"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="2.7.2.rc1-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-v2.7.2-linux-x86_64.tar.gz"
    alrb_InstallAlternateNames=( "2.7.2-x86_64-slc6" )
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.10.0-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-v2.10.0-linux-x86_64.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.11.1-x86_64-slc6"
#    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-2.11.1-x86_64-slc6.tgz"
#    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-2.11.1.patch1-x86_64-slc6.tgz"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-2.11.1.patch2-x86_64-slc6.tgz"
    alrb_ToolInstallTag="1492012831"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.11.1-x86_64-centos7"
#    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-2.11.1-x86_64-centos7.tgz"
#    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-2.11.1.patch1-x86_64-centos7.tgz"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-2.11.1.patch2-x86_64-centos7.tgz"
    alrb_ToolInstallTag="1492012831"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.26.2-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-${alrb_InstallVersion}.tgz"
    alrb_ToolInstallTag="1590781118"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.26.2.p1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-${alrb_InstallVersion}.tgz"
    alrb_ToolInstallTag="1599672210"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.39.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.41.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

