# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="5.7.3-aarch64-el9"
alrb_InstallTesting="5.7.3-aarch64-el9"
alrb_InstallPrevious="5.7.2-aarch64-el9"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL8="5.7.3-aarch64-centos8"
alrb_InstallTestingSL8="5.7.3-aarch64-centos8"
alrb_InstallPreviousSL8="5.7.2-aarch64-centos8"

# frozen
alrb_InstallDefaultSL7="5.4.2-aarch64-centos7"
alrb_InstallTestingSL7="5.4.2-aarch64-centos7"
alrb_InstallPreviousSL7="5.4.2-aarch64-centos7"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="5.3.4-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/5.3.4-aarch64-centos7.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.0-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/5.4.0-aarch64-centos7.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.2-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.2-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.2-fix1-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.3-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.3-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_ToolInstallTag="1659503529"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.0-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.0-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.0-fix1-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.0-fix1-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.1-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.1-fix1-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.2-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.3-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.4-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.5-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.0-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.1-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.2-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.3-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.3-fix1-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.4-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.4-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.6-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.6-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.9-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.9-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.0-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.0-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.1-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.1-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.2-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.2-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.3-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.3-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

