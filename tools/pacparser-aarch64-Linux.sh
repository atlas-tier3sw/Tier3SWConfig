# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="1.4.1-aarch64-centos9"
alrb_InstallTesting="1.4.1-aarch64-centos9"
alrb_InstallPrevious="1.4.1-aarch64-centos9"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL8="1.4.1-fix1-aarch64-centos8"
alrb_InstallTestingSL8="1.4.1-fix1-aarch64-centos8"
alrb_InstallPreviousSL8="1.4.1-fix1-aarch64-centos8"

# obsolete but we still need to suppoort it ...
alrb_InstallDefaultSL7="1.4.1-aarch64-centos7"
alrb_InstallTestingSL7="1.4.1-aarch64-centos7"
alrb_InstallPreviousSL7="1.4.1-aarch64-centos7"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="1.4.1-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/pacparser/v$alrb_InstallVersion.tar.gz"
    alrb_InstallAlternateNames=( "1.4.1-aarch64-el9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.4.1-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/pacparser/v$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.4.1-fix1-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/pacparser/v$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

