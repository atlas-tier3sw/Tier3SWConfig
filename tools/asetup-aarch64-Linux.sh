# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="V03-01-14"  

alrb_InstallTesting="V03-01-14"

alrb_InstallPrevious="V03-01-11"

alrb_InstallPlatform="aarch64-Linux"


(
    alrb_InstallVersion="V02-02-06"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V02-02-07"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V02-02-10"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-00-00"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-00-01"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-00-06"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-00-07"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-00-09"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-00-10"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-00-11"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-00-12"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-00-14"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-00-15"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-00-16"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-00-17"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-00-18"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-01-01"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-01-02"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-01-04"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-01-05"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-01-07"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-01-09"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-01-10"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-01-11"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-01-12"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-01-13"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_InstallAction='remove'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="V03-01-14"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/AtlasSetup/AtlasSetup-`\echo $alrb_InstallVersion | \sed 's/^V//'`.tgz"
    alrb_fn_installAction
)


