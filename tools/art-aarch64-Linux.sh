# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="1.6.16"
alrb_InstallTesting="1.6.16"
alrb_InstallPrevious="1.6.14"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="1.5.10"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/art/art-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.21"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/art/art-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.5.31"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/art/art-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.6.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/art/art-sw-${alrb_InstallVersion}.tar.gz"
    alrb_InstallAction='obsolete'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="1.6.0-fix1"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/art/art-1.6.0.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.6.2"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/art/art-${alrb_InstallVersion}.tar.gz"
    alrb_ToolInstallTag="1686671915"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.6.14"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/art/art-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.6.15"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/art/art-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.6.16"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/art/art-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)


