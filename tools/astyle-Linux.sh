# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="00.05.04"
alrb_InstallPrevious="00.05.03"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="00.05.00"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/astyle/atlasrootstyle-${alrb_InstallVersion}.tar"
    alrb_ToolInstallTag="1664901090"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="00.05.01"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/astyle/atlasrootstyle-${alrb_InstallVersion}.tar"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="00.05.02"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/astyle/atlasrootstyle-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="00.05.03"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/astyle/atlasrootstyle-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="00.05.04"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/astyle/atlasrootstyle-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)
