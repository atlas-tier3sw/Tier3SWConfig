# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="3.27.5"
alrb_InstallTesting="3.31.5"
alrb_InstallPrevious="3.26.4"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="3.18.3"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-3.18.3-Linux-aarch64.tar.gz"
    alrb_ToolInstallTag="1603902767"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.20.2"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-aarch64.tar.gz"
    alrb_ToolInstallTag="1620063941"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.21.3"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-aarch64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.24.3"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-aarch64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.26.4"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-aarch64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.27.5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-aarch64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.28.4"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-aarch64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.29.2"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-aarch64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.29.5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-aarch64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.30.5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-aarch64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.31.3"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-aarch64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.31.5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-aarch64.tar.gz"
    alrb_fn_installAction
)


