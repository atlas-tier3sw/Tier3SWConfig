# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallRecommended="5.0.0-aarch64-centos9"
alrb_InstallRecommendedSL9="$alrb_InstallRecommended"

alrb_InstallTesting="$alrb_InstallRecommended"
alrb_InstallTestingSL9="$alrb_InstallTesting"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="5.0.0-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/scikit/v$alrb_InstallVersion.tar.gz"
    alrb_InstallAlternateNames=( "5.0.0-aarch64-el9" )
    alrb_fn_installAction
)

