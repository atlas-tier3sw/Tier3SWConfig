# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefaultSL9="1.85.0-aarch64-el9"
alrb_InstallTestingSL9="1.85.0-aarch64-el9"
alrb_InstallPreviousSL9="1.82.0-aarch64-centos9"

alrb_InstallDefaultSL8="1.85.0-aarch64-centos8"
alrb_InstallTestingSL8="1.85.0-aarch64-centos8"
alrb_InstallPreviousSL8="1.82.0-aarch64-centos8"


alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="1.82.0-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/boost/$alrb_InstallVersion.tgz"    
    alrb_ToolInstallTag="1689376055"
    alrb_InstallAlternateNames=( "1.82.0-aarch64-el9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.82.0-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/boost/$alrb_InstallVersion.tgz"    
    alrb_ToolInstallTag="1700855042"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.85.0-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/boost/$alrb_InstallVersion.tgz"    
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.85.0-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/boost/$alrb_InstallVersion.tgz"    
    alrb_ToolInstallTag="1723565618"
    alrb_fn_installAction
)

