# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="19.1.7-aarch64-el9-gcc13-opt"
alrb_InstallTesting="19.1.7-aarch64-el9-gcc13-opt"
alrb_InstallPrevious="17.0.6-aarch64-el9-gcc13-opt"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="17.0.6-aarch64-el9-gcc13-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="19.1.7-aarch64-el9-gcc13-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)


