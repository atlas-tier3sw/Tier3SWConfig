# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="24.08.14-snapshot-aarch64-el9"
alrb_InstallTesting="24.08.14-snapshot-aarch64-el9"
alrb_InstallPrevious="24.05.22-snapshot-aarch64-el9"

alrb_InstallPreviousSL9="$alrb_InstallPrevious"
alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"

alrb_InstallDefaultSL8="24.08.14-snapshot-aarch64-centos8"
alrb_InstallTestingSL8="24.08.14-snapshot-aarch64-centos8"
alrb_InstallPreviousSL8="24.05.22-snapshot-aarch64-centos8"

# frozen
alrb_InstallDefaultSL7="c7-armuitools-130122-aarch64-centos7"
alrb_InstallTestingSL7="c7-armuitools-130122-aarch64-centos7"
alrb_InstallPreviousSL7="c7-armuitools-130122-aarch64-centos7"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="c7-armuitools-130122-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/c7-armuitools-130122-aarch64-centos7.tgz"
    alrb_ToolInstallTag="1642123157"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.03.14-snapshot-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/${alrb_InstallVersion}.tgz"
    alrb_ToolInstallTag="1710444820"
    alrb_InstallAction='remove'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.03.14-snapshot-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/${alrb_InstallVersion}.tgz"
    alrb_ToolInstallTag="1710444820"
    alrb_InstallAction='remove'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.05.22-snapshot-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.05.22-snapshot-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.08.14-snapshot-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.08.14-snapshot-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

