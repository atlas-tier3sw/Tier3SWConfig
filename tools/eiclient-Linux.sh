# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="4.3.2"
alrb_InstallTesting="4.3.2"
alrb_InstallPrevious="4.3.2"

alrb_InstallPlatform="any"

(
    alrb_InstallVersion="4.3.2"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/eiclient/ei-bundle-${alrb_InstallVersion}-scripts.tar.bz2"
    alrb_fn_installAction
)
