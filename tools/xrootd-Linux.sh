# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="5.7.3-x86_64-el9"
alrb_InstallTesting="5.7.3-x86_64-el9"
alrb_InstallPrevious="5.7.2-x86_64-el9"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL8="5.7.3-x86_64-centos8"
alrb_InstallTestingSL8="5.7.3-x86_64-centos8"
alrb_InstallPreviousSL8="5.7.2-x86_64-centos8"

alrb_InstallDefaultSL7="5.7.3-x86_64-centos7"
alrb_InstallTestingSL7="5.7.3-x86_64-centos7"
alrb_InstallPreviousSL7="5.7.2-x86_64-centos7"

# frozen
alrb_InstallDefaultSL6="5.0.3-x86_64-slc6"
alrb_InstallTestingSL6="$alrb_InstallDefaultSL6"
alrb_InstallPreviousSL6="$alrb_InstallDefaultSL6"

# frozen
alrb_InstallDefaultSL5="4.5.0-x86_64-slc5"
alrb_InstallTestingSL5="$alrb_InstallDefaultSL5"
alrb_InstallPreviousSL5="$alrb_InstallDefaultSL5"


alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="3.1.0-i686-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.1.0.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.1.0-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.1.0.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.1.1-i686-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.1.1.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.1.1-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.1.1.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.2.2-i686-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.2.2.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.2.2-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.2.2.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.2.2-i686-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.2.2.rhel6-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.2.2-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.2.2.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.2.4-i686-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.2.4.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.2.4-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.2.4.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.2.7-i686-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.2.7.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.2.7-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.2.7.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.2.7-i686-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.2.7.rhel6-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.2.7-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.2.7.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.3.4-i686-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.3.4.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.3.4-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.3.4.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.3.4-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.3.4.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.3.5-i686-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.3.5.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.3.5-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.3.5.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.3.5-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.3.5.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.3.6-i686-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.3.6.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.3.6-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.3.6.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.3.6-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.3.6.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.3.6.p1-x86_64-slc6-gcc48-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3.3.6.p1-x86_64-slc6-gcc48-opt.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.0.2-i686-slc5"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.0.2.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.0.2-x86_64-slc5"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.0.2.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.0.2-x86_64-slc6"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.0.2.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.0.3-i686-slc5"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.0.3.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.0.3-x86_64-slc5"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.0.3.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.0.3-x86_64-slc6"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.0.3.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.0.4-i686-slc5"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.0.4.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.0.4-x86_64-slc5"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.0.4.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.0.4-x86_64-slc6"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.0.4.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.1.0-i686-slc5"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.1.0.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.1.0-x86_64-slc5"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.1.0.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.1.0-x86_64-slc6"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.1.0.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.1.1-i686-slc5"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.1.1.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.1.1-x86_64-slc5"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.1.1.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.1.1-x86_64-slc6"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.1.1.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.1.2-i686-slc5"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.1.2.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.1.2-x86_64-slc5"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.1.2.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.1.2-x86_64-slc6"
    alrb_InstallTarballDownload="atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.1.2.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

# 4.2.0
# note that python bindings are missing in this version so this is a snapshot
# savec on the atcan mirror

(
    alrb_InstallVersion="4.2.0-i686-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.2.0.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.2.0-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.2.0.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.2.0-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.2.0.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.2.2-i686-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.2.2.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.2.2-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.2.2.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.2.2-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.2.2.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.2.3-i686-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.2.3.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.2.3-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.2.3.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.2.3-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.2.3.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.3.0-i686-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.3.0.rhel5-32_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.3.0-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.3.0.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.3.0-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.3.0.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.4.0-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.4.0.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.4.0-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.4.0.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.4.1-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.4.1.rhel5-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.4.1-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.4.1.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.5.0-x86_64-slc5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.5.0.rhel5-64_dbg.tar.gz"
#    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.5.0-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.5.0.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.5.0-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-3a57f_4.5.0_x86_64_slc6_gcc62_opt.tgz"
    alrb_ToolInstallTag="1497044683"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.5.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.5.0.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.6.1-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.6.1.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.6.1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.6.1.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.6.0-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-ef575_4.6.0_x86_64_slc6_gcc62_opt.tgz"
    alrb_ToolInstallTag="1497044683"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.6.1-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-5209d_4.6.1_x86_64_slc6_gcc62_opt.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.7.0-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.7.0.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.7.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.7.0.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.7.0-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-f5f6a_4.7.0_x86_64_slc6_gcc62_opt-1.0.0-92.noarch.rpm.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.7.1-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.7.1.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.7.1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.7.1.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.8.0-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.8.0.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.8.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.8.0.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.8.1-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.8.1.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.8.1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.8.1.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.8.2-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.8.2.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.8.2-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.8.2.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.8.3-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.8.3.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.8.3-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.8.3.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.8.4-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.8.4.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.8.4-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.8.4.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.8.5-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.8.5.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.8.5-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.8.5.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.9.0-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.9.0.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.9.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.9.0.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.9.1-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.9.1.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.9.1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.9.1.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.11.1-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.11.1.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.11.1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.11.1.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.11.2-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.11.2.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.11.2-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.11.2.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.11.3-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.11.3.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.11.3-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.11.3.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.12.0-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.12.0.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.12.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.12.0.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.12.1-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.12.1.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.12.1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.12.1.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.12.2-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.12.2.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.12.2-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.12.2.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.12.3-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.12.3.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.12.3-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.12.3.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="4.12.4-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.12.4.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="4.12.4-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-4.12.4.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.0.2-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-5.0.2.rhel6-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.0.2-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-5.0.2.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.0.3-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-5.0.3.rhel6-64_dbg.tar.gz"
#    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.0.3-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-5.0.3.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.1.1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-5.1.1.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.2.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-5.2.0.rhel7-64_dbg.tar.gz"
    alrb_ToolInstallTag="1622567972"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.3.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-5.3.0.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.3.1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-5.3.1.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.3.2-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-5.3.2.rhel7-64_dbg.tar.gz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.3.4-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-5.3.4.rhel7-64_dbg.tar.gz"
    alrb_ToolInstallTag="1638397270"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/xrootd-5.4.0.rhel7-64_dbg.tar.gz"
    alrb_ToolInstallTag="1638397270"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.0-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/5.4.0-x86_64-centos8.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.2-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.2-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.2-fix1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.2-fix1-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.3-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_ToolInstallTag="1654800482"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.3-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.4.3-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_ToolInstallTag="1659503529"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.0-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.0-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.0-fix1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.0-fix1-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.0-fix1-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.1-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.1-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.1-fix1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.1-fix1-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.1-fix1-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.2-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.2-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.2-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.3-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.3-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.3-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.4-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.4-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.4-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.5-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.5-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.5.5-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.0-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.0-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.1-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.1-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.2-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.2-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.2-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.3-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.3-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.3-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.3-fix1-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.4-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.4-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.4-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.6-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"    
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.6-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.6-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.9-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.9-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.6.9-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.0-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.0-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.1-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.1-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.2-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.2-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.2-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.3-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.3-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.7.3-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xrootd/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

