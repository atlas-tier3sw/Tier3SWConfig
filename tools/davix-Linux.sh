# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="0.8.7-x86_64-el9"
alrb_InstallTesting="0.8.7-x86_64-el9"
alrb_InstallPrevious="0.8.6-x86_64-el9"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL8="0.8.7-x86_64-centos8"
alrb_InstallTestingSL8="0.8.7-x86_64-centos8"
alrb_InstallPreviousSL8="0.8.6-x86_64-centos8"

alrb_InstallDefaultSL7="0.8.7-x86_64-centos7"
alrb_InstallTestingSL7="0.8.7-x86_64-centos7"
alrb_InstallPreviousSL7="0.8.6-x86_64-centos7"

# frozen
alrb_InstallDefaultSL6="0.7.6-x86_64-slc6"
alrb_InstallTestingSL6="0.7.6-x86_64-slc6"
alrb_InstallPreviousSL6="0.7.6-x86_64-slc6"

alrb_InstallPlatform="x86_64"


(
    alrb_InstallVersion="0.3.6-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.3.6.linux.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.4.0-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.4.0.linux.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.6.0-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.6.0.el6.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.6.2-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/Davix-d4526_0.6.2_x86_64_slc6_gcc62_opt.tgz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_ToolInstallTag="1497044683"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.6.3-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.6.3.el6.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.6.4-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.6.4.el6.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.6.4-x86_64-slc7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.6.4.el7.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.6.6-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.6.6.el6.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.6.6-x86_64-slc7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.6.6.el7.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.6.7-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.6.7.el6.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.6.7-x86_64-slc7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.6.7.el7.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.6.7-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/Davix-0.6.7_00b8f-x86_64-slc6-gcc62-opt.tgz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="0.6.8-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.6.8-el6.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.6.8-x86_64-slc7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.6.8-el7.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="0.7.1-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.1-el6.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.7.1-x86_64-slc7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.1-el7.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="0.7.2-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.2-el6.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.7.2-x86_64-slc7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.2-el7.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="0.7.3-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.3-el6.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.7.3-x86_64-slc7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.3-el7.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="0.7.4-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.4-el6.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.7.4-x86_64-slc7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.4-el7.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.7.4-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.4-el6.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.7.4-x86_64-slc7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.4-el7.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)


(
    alrb_InstallVersion="0.7.5-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.5-el6.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.7.5-x86_64-slc7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.5-el7.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.7.6-x86_64-slc6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.6-el6.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.7.6-x86_64-slc7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-0.7.6-el7.x86_64.tar.gz"
    alrb_InstallTarballOptions="--strip-components=1"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.0-x86_64-centos7"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_0-x86_64-centos7.tgz"
       alrb_InstallAction='archive'
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.1-x86_64-centos8"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_1-x86_64-centos8.tar.gz"
       alrb_ToolInstallTag="1648842049"
       alrb_InstallAction='archive'
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.1-x86_64-centos7"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_1-x86_64-centos7.tar.gz"
       alrb_ToolInstallTag="1648842049"
       alrb_InstallAction='archive'
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.2-x86_64-centos8"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_2-x86_64-centos8.tar.gz"
       alrb_InstallAction='archive'
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.2-x86_64-centos7"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_2-x86_64-centos7.tar.gz"
       alrb_InstallAction='archive'
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.2-x86_64-centos9"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_2-x86_64-centos9.tar.gz"
       alrb_InstallAction='archive'
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.3-x86_64-centos8"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_3-x86_64-centos8.tar.gz"
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.3-x86_64-centos7"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_3-x86_64-centos7.tar.gz"
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.3-x86_64-centos9"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_3-x86_64-centos9.tar.gz"
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.4-x86_64-centos8"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_4-x86_64-centos8.tar.gz"
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.4-x86_64-centos7"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_4-x86_64-centos7.tar.gz"
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.4-x86_64-centos9"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_4-x86_64-centos9.tar.gz"
       alrb_InstallAlternateNames=( "0.8.4-x86_64-el9" )
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.5-x86_64-centos8"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.5-x86_64-centos7"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.5-x86_64-el9"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.6-x86_64-centos7"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.6-x86_64-centos8"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.6-x86_64-el9"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.7-x86_64-centos7"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.7-x86_64-centos8"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
       alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.7-x86_64-el9"
       alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
       alrb_fn_installAction
)


