# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="pyAMI-5.1.7"
alrb_InstallTesting="pyAMI-5.1.7"
alrb_InstallPrevious="pyAMI-5.1.6"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="pyAMI-5.1.2"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/pyAmi/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="pyAMI-5.1.3"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/pyAmi/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="pyAMI-5.1.4"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/pyAmi/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="pyAMI-5.1.5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/pyAmi/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="pyAMI-5.1.6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/pyAmi/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="pyAMI-5.1.7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/pyAmi/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

