# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="3.27.5"
alrb_InstallTesting="3.31.5"
alrb_InstallPrevious="3.26.4"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="3.7.0"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.7/cmake-3.7.0-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.8.1"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.8/cmake-3.8.1-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.6"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.9/cmake-3.9.6-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.10.3"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.10/cmake-3.10.3-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.0"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.11/cmake-3.11.0-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.3"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.11/cmake-3.11.3-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.0"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.12/cmake-3.12.0-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.2"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.12/cmake-3.12.2-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.3"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.12/cmake-3.12.3-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.13.0"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.13/cmake-3.13.0-Linux-x86_64.tar.gz"
    alrb_ToolInstallTag="1543335039"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.13.1"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.13/cmake-3.13.1-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.13.3"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.13/cmake-3.13.3-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.14.0"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.14/cmake-3.14.0-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.14.1"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.14/cmake-3.14.1-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.14.3"
    alrb_InstallTarballDownload="https://cmake.org/files/v3.14/cmake-3.14.3-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)


(
    alrb_InstallVersion="3.14.7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-3.14.7-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.15.4"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-3.15.4-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.15.5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-3.15.5-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.16.2"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-3.16.2-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.16.3"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-3.16.3-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.16.4"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-3.16.4-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.17.3"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.18.3"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-Linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.20.2"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-x86_64.tar.gz"
    alrb_ToolInstallTag="1620063941"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.21.3"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.24.3"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.26.4"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.27.5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.28.4"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.29.2"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.29.5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.30.5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.31.3"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-x86_64.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.31.5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cmake/cmake-${alrb_InstallVersion}-linux-x86_64.tar.gz"
    alrb_fn_installAction
)


