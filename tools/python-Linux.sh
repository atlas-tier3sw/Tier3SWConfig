# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallPilot="3.11.10-x86_64-el9"
alrb_InstallPilotTesting="3.12.9-x86_64-el9"
alrb_InstallRecommended="3.9.21-x86_64-el9"

alrb_InstallPilotSL9="$alrb_InstallPilot"
alrb_InstallPilotTestingSL9="$alrb_InstallPilotTesting"
alrb_InstallRecommendedSL9="$alrb_InstallRecommended"

alrb_InstallPilotSL8="3.11.10-x86_64-centos8"
alrb_InstallPilotTestingSL8="3.12.9-x86_64-centos8"
alrb_InstallRecommendedSL8="3.9.21-x86_64-centos8"

alrb_InstallPilotSL7="3.9.20-x86_64-centos7"
alrb_InstallPilotTestingSL7="3.9.21-x86_64-centos7"
alrb_InstallRecommendedSL7="3.9.21-x86_64-centos7"

# 2.7 needed for panda clients on slc6 
alrb_InstallRecommendedSL6="2.7.18-x86_64-centos6"

alrb_InstallPlatform="x86_64"

alrb_InstallRelocateFilesAr=( "setup.sh" "setup.csh" )

alrb_InstallPacmanMirror="http://atlas-computing.web.cern.ch/atlas-computing/links/kitsDirectory/projects/cache"

(
    alrb_InstallVersion="2.5.4p2-x86_64-slc5-gcc43"
    alrb_InstallPacmanDownload="Python_2_5_4p2_x86_64_slc5_gcc43_opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.5.4p2-i686-slc5-gcc43"
    alrb_InstallPacmanDownload="Python_2_5_4p2_i686_slc5_gcc43_opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.6.5-x86_64-slc5-gcc43"
    alrb_InstallPacmanDownload="Python_2_6_5_x86_64_slc5_gcc43_opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.6.5-i686-slc5-gcc43"
    alrb_InstallPacmanDownload="Python_2_6_5_i686_slc5_gcc43_opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.6.5p1-i686-slc5-gcc43"
    alrb_InstallPacmanDownload="Python_2_6_5p1_i686_slc5_gcc43_opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.6.5p1-x86_64-slc5-gcc43"
    alrb_InstallPacmanDownload="Python_2_6_5p1_x86_64_slc5_gcc43_opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.3-i686-slc6-gcc47"
    alrb_InstallPacmanDownload="Python_2_7_3_i686_slc6_gcc47_opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.3-x86_64-slc6-gcc47"
    alrb_InstallPacmanDownload="Python_2_7_3_x86_64_slc6_gcc47_opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.4-x86_64-slc6-gcc48"
    alrb_InstallPacmanDownload="Python_2_7_4_x86_64_slc6_gcc48_opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.9-x86_64-slc6-gcc48"
    alrb_InstallPacmanDownload="Python_2_7_9_x86_64_slc6_gcc48_opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.9p1-x86_64-slc6-gcc49"
    alrb_InstallPacmanDownload="Python_2_7_9_p1_lcgcmt81b_x86_64_slc6_gcc49_opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.10-x86_64-slc6-gcc49"
    alrb_InstallPacmanDownload="Python_2_7_10_lcgcmt84_x86_64_slc6_gcc49_opt"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.10-x86_64-slc6-gcc62"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/Python-806f9_2.7.10_x86_64_slc6_gcc62_opt.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.13-x86_64-slc6-gcc49"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/Python-597a5_2.7.13_x86_64_slc6_gcc49_opt.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.13-x86_64-slc6-gcc62"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/Python-597a5_2.7.13_x86_64_slc6_gcc62_opt.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.13-x86_64-slc6-gcc44"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/Python-2.7.13_b163d-x86_64-slc6-gcc44-opt.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)


(
# special version for Paul's pilot tests
    alrb_InstallVersion="3.7.4-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/Python-3.7.4_x86_64_centos7-gcc485.tgz"
#    alrb_InstallAlternateNames=( "pilotTest" )
    alrb_ToolInstallTag="1568838222"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
# special version for Paul's pilot tests
    alrb_InstallVersion="3.8.8-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/Python-3.8.8_x86_64_centos7-gcc485.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
# special version for Paul's pilot tests
    alrb_InstallVersion="3.9.7-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/Python-3.9.7_x86_64_centos7-gcc485.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.7.13-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAction="archive"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.8.13-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAction="archive"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.11-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAction="archive"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.7.13-fix1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/3.7.13-x86_64-centos7.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.8.13-fix1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/3.8.13-x86_64-centos7.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.11-fix1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/3.9.11-x86_64-centos7.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.13-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.7.13-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/3.7.13-x86_64-centos8.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.8.13-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/3.8.13-x86_64-centos8.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.11-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/3.9.11-x86_64-centos8.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.13-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAction='attic'    
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.13-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.14-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos7-3.9" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.14-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.9" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.14-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.9" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.10.7-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.10.7-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.10.7-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.18-x86_64-centos6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_ToolInstallTag="1670025892"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.3-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.4-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.11" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.18-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos7-2.7" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7.18-fix1-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAlternateNames=( "centos7-2.7" )
    alrb_ToolInstallTag="1695339945"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.18-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos7-3.9" )
    alrb_ToolInstallTag="1695339945"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.18-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.9" )
    alrb_ToolInstallTag="1695339945"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.18-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.9" "el9-3.9" )
    alrb_ToolInstallTag="1695339945"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.19-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos7-3.9" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.19-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.9" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.19-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.9" "el9-3.9" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.20-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos7-3.9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.20-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.20-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.9" "el9-3.9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.21-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAlternateNames=( "centos7-3.9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.21-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAlternateNames=( "centos8-3.9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.9.21-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAlternateNames=( "centos9-3.9" "el9-3.9" )
    alrb_fn_installAction
)


(
    alrb_InstallVersion="3.11.5-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.11" "el9-3.11" )
    alrb_ToolInstallTag="1695339945"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.6-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.11" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.7-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.11" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.7-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.11" "el9-3.11" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.9-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.11" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.9-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.11" "el9-3.11" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.10-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAlternateNames=( "centos8-3.11" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.11.10-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAlternateNames=( "centos9-3.11" "el9-3.11" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.4-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.4-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.5-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.12" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.5-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.12" "el9-3.12" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.6-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.12" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.6-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.12" "el9-3.12" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.7-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.12" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.7-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.12" "el9-3.12" )
    alrb_InstallAction='attic'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.8-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos8-3.12" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.8-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
#    alrb_InstallAlternateNames=( "centos9-3.12" "el9-3.12" )
    alrb_fn_installAction
)


(
    alrb_InstallVersion="3.12.9-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAlternateNames=( "centos8-3.12" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.12.9-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/Python/${alrb_InstallVersion}.tgz"
    alrb_InstallAlternateNames=( "centos9-3.12" "el9-3.12" )
    alrb_fn_installAction
)
