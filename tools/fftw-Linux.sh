# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallPlatform="x86_64"
alrb_InstallAction='archive'

(
    alrb_InstallVersion="3.1.2-x86_64-slc6-gcc48-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/fftw/fftw-$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.1.2-x86_64-slc6-gcc49-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/fftw/fftw-$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.3.4-x86_64-slc6-gcc49-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/fftw/fftw-$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="3.3.4-x86_64-slc6-gcc62-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/fftw/fftw-$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

