# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="1.0.32"  
alrb_InstallTesting="$alrb_InstallDefault"
alrb_InstallPrevious="1.0.31"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="1.0.31"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xcache/xcache-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="1.0.32"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/xcache/xcache-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

