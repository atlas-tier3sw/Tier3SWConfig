# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="0.4.6"
alrb_InstallPrevious="0.4.5"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="0.3.2"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/adctools/atlas-adc-tools-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.3.3"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/adctools/atlas-adc-tools-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.4.2"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/adctools/atlas-adc-tools-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.4.4"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/adctools/atlas-adc-tools-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.4.5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/adctools/atlas-adc-tools-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.4.6"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/adctools/atlas-adc-tools-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)
