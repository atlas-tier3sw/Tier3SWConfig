# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="v10.3.0"
alrb_InstallTesting="v10.3.0"
alrb_InstallPrevious="v10.2.0"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="v10.1.1"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/atlantis/atlantis-${alrb_InstallVersion}.tgz"
    alrb_InstallAction="obsolete"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="v10.1.1-fix1"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/atlantis/atlantis-${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="v10.2.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/atlantis/atlantis-${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="v10.3.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/atlantis/atlantis-${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

