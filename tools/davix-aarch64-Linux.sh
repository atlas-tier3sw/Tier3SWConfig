# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="0.8.7-aarch64-el9"
alrb_InstallTesting="0.8.7-aarch64-el9"
alrb_InstallPrevious="0.8.6-aarch64-el9"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL8="0.8.7-aarch64-centos8"
alrb_InstallTestingSL8="0.8.7-aarch64-centos8"
alrb_InstallPreviousSL8="0.8.6-aarch64-centos8"

# sl7 is no longer supported on this platform
alrb_InstallDefaultSL7="0.8.0-aarch64-centos7"
alrb_InstallTestingSL7="0.8.0-aarch64-centos7"
alrb_InstallPreviousSL7="0.7.5"

alrb_InstallPlatform="aarch64-Linux"


(
    alrb_InstallVersion="0.7.5"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/davix-aarch64-dummy-v1.tgz"
    alrb_ToolInstallTag="1642177120"
    alrb_fn_installAction
)


(
    alrb_InstallVersion="0.8.0-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_0-aarch64-centos7.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.1-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_1-aarch64-centos8.tar.gz"
    alrb_ToolInstallTag="1648842049"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.2-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_2-aarch64-centos8.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.2-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_2-aarch64-centos9.tar.gz"
    alrb_InstallAction='archive'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.3-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_3-aarch64-centos9.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.4-aarch64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/R_0_8_4-aarch64-centos9.tar.gz"
    alrb_InstallAlternateNames=( "0.8.4-aarch64-el9" )
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.4-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
    alrb_ToolInstallTag="1700687433"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.5-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.5-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.6-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.6-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.7-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.8.7-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/davix/${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

