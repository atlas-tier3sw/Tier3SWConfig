# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="0.1.30"

alrb_InstallTesting="0.1.30"

alrb_InstallPrevious="0.1.29"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="0.1.27"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/acm/acm-${alrb_InstallVersion}.tar"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.1.28"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/acm/acm-${alrb_InstallVersion}.tar"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.1.29"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/acm/acm-${alrb_InstallVersion}.tar"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.1.30"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/acm/acm-${alrb_InstallVersion}.tar"
    alrb_ToolInstallTag="1721315549"
    alrb_fn_installAction
)

