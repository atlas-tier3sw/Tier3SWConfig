# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="2.14-x86_64-el9-gcc13"
alrb_InstallTesting="2.14-x86_64-el9-gcc13"
alrb_InstallPrevious="2.12-x86_64-el9-gcc11"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL7="2.14-x86_64-centos7-gcc11"
alrb_InstallPreviousSL7="2.12-x86_64-centos7-gcc11"
alrb_InstallTestingSL7="2.14-x86_64-centos7-gcc11"

# frozen
alrb_InstallDefaultSL6="1.85-x86_64-slc6-gcc62"  
alrb_InstallTestingSL6="1.85-x86_64-slc6-gcc62"
alrb_InstallPreviousSL6="1.85-x86_64-slc6-gcc62" 

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="1.85-x86_64-slc6-gcc62"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/cppcheck-1.85-x86_64-slc6-gcc62.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.6-x86_64-centos7-gcc11"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.6.tar.gz"
    alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_v1.sh"
    alrb_ToolInstallTag="1641512324"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.7-x86_64-centos7-gcc11"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.7.tar.gz"
    alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.8-x86_64-centos7-gcc11"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.8.tar.gz"
    alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.9-x86_64-centos7-gcc11"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.9.tar.gz"
    alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_v1.sh"
    alrb_ToolInstallTag="1661786983"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.10-x86_64-centos7-gcc11"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.10.tar.gz"
    alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.11-x86_64-centos7-gcc11"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.11.1.tar.gz"
    alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc11_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.11-x86_64-centos7-gcc13"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.11.1.tar.gz"
    alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc13_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.11-x86_64-el9-gcc11"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.11.1.tar.gz"
    alrb_PostInstallUseContainer="el9|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc11_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.11-x86_64-el9-gcc13"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.11.1.tar.gz"
    alrb_PostInstallUseContainer="el9|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc13_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.12-x86_64-centos7-gcc11"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.12.0.tar.gz"
    alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc11_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.12-x86_64-centos7-gcc13"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.12.0.tar.gz"
    alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc13_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.12-x86_64-el9-gcc11"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.12.0.tar.gz"
    alrb_PostInstallUseContainer="el9|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc11_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.12-x86_64-el9-gcc13"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.12.0.tar.gz"
    alrb_PostInstallUseContainer="el9|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc13_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.14-x86_64-centos7-gcc11"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.14.1.tar.gz"
    alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc11_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.14-x86_64-centos7-gcc13"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.14.1.tar.gz"
    alrb_PostInstallUseContainer="centos7|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc13_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.14-x86_64-el9-gcc11"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.14.1.tar.gz"
    alrb_PostInstallUseContainer="el9|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc11_v1.sh"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.14-x86_64-el9-gcc13"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cppcheck/2.14.1.tar.gz"
    alrb_PostInstallUseContainer="el9|$mt3sw_configDir/scripts/cppcheck/postInstall_gcc13_v1.sh"
    alrb_fn_installAction
)

