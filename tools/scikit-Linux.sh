# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallRecommended="5.0.0-x86_64-centos9"
alrb_InstallRecommendedSL9="$alrb_InstallRecommended"

alrb_InstallTesting="$alrb_InstallRecommended"
alrb_InstallTestingSL9="$alrb_InstallTesting"

# frozen
alrb_InstallRecommendedSL7="5.0.0-x86_64-centos7"
alrb_InstallTestingSL7="$alrb_InstallRecommendedSL7"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="5.0.0-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/scikit/v$alrb_InstallVersion.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="5.0.0-x86_64-centos9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/scikit/v$alrb_InstallVersion.tar.gz"
    alrb_InstallAlternateNames=( "5.0.0-x86_64-el9" )
    alrb_fn_installAction
)

