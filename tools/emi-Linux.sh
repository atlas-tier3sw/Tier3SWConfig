# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="24.08.14-snapshot-x86_64-el9"
alrb_InstallTesting="24.08.14-snapshot-x86_64-el9"
alrb_InstallPrevious="24.05.22-snapshot-x86_64-el9"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL8="24.08.14-snapshot-x86_64-centos8"
alrb_InstallTestingSL8="24.08.14-snapshot-x86_64-centos8"
alrb_InstallPreviousSL8="24.05.22-snapshot-x86_64-centos8"

alrb_InstallDefaultSL7="24.08.15-snapshot-x86_64-centos7"
alrb_InstallTestingSL7="24.08.15-snapshot-x86_64-centos7"
alrb_InstallPreviousSL7="24.05.22-snapshot-x86_64-centos7"

# frozen
alrb_InstallDefaultSL6="3.17.1-1.el6umd4v5.fix1a"
alrb_InstallTestingSL6="3.17.1-1.el6umd4v5.fix1a"
alrb_InstallPreviousSL6="3.17.1-1.el6umd4v5.fix1a"

alrb_InstallPlatform="x86_64"


(
    # fix1a is the same as fix1 but removed the vomses file for atlas-voms-atlas-auth.app.cern.ch.
    alrb_InstallVersion="3.17.1-1.el6umd4v5.fix1a"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/emi-ui-3.17.1-1.el6umd4v5.fix1a-relocatable.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.03.14-snapshot-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/$alrb_InstallVersion.tgz"
    alrb_ToolInstallTag="1710444820"
    alrb_InstallAction='remove'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.03.14-snapshot-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/$alrb_InstallVersion.tgz"
    alrb_ToolInstallTag="1710444820"
    alrb_InstallAction='remove'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.03.14-snapshot-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/$alrb_InstallVersion.tgz"
    alrb_ToolInstallTag="1710444820"
    alrb_InstallAction='remove'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.05.22-snapshot-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.05.22-snapshot-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.05.22-snapshot-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.08.14-snapshot-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/$alrb_InstallVersion.tgz"
    alrb_InstallAction='remove'
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.08.15-snapshot-x86_64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.08.14-snapshot-x86_64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="24.08.14-snapshot-x86_64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/emi/$alrb_InstallVersion.tgz"
    alrb_fn_installAction
)


