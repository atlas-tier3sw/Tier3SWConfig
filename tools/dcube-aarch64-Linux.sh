# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="7.3.0"
alrb_InstallTesting="7.3.0"
alrb_InstallPrevious="7.2.0"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="210903"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="220916"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="230403"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="230421"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="7.0.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="7.1.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="7.2.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="7.3.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/dcube/dcube-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

