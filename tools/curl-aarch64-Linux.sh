# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="8.7.1-aarch64-el9"
alrb_InstallPrevious="8.7.1-aarch64-el9"
alrb_InstallTesting="8.7.1-aarch64-el9"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"
alrb_InstallTestingSL9="$alrb_InstallTesting"

alrb_InstallDefaultSL8="8.7.1-aarch64-centos8"
alrb_InstallPreviousSL8="8.7.1-aarch64-centos8"
alrb_InstallTestingSL8="8.7.1-aarch64-centos8"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="8.7.1-aarch64-centos8"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/curl/${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="8.7.1-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/curl/${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)
