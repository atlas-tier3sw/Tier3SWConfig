# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="7.5.0-aarch64-el9"
alrb_InstallPrevious="7.5.0-aarch64-el9"
alrb_InstallTesting="7.5.0-aarch64-el9"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"
alrb_InstallTestingSL9="$alrb_InstallTesting"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="7.5.0-aarch64-el9"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/varnish/${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)
