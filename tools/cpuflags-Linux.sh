# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="0.1.0"
alrb_InstallPrevious="0.0.2"
alrb_InstallTesting="0.1.0"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="0.0.2"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cpuflags/cpu_flags-v${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="0.1.0"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/cpuflags/cpu_flags-v${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)
