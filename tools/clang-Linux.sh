# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="17.0.6-x86_64-el9-gcc13-opt"
alrb_InstallTesting="18.1.3-x86_64-el9-gcc13-opt"
alrb_InstallPrevious="17.0.6-x86_64-el9-gcc13-opt"

alrb_InstallDefaultSL9="$alrb_InstallDefault"
alrb_InstallTestingSL9="$alrb_InstallTesting"
alrb_InstallPreviousSL9="$alrb_InstallPrevious"

alrb_InstallDefaultSL7="16.0.3-x86_64-centos7-gcc11-opt"
alrb_InstallTestingSL7="16.0.3-x86_64-centos7-gcc11-opt"
alrb_InstallPreviousSL7="15.0.4-x86_64-centos7-gcc11-opt"

alrb_InstallPlatform="x86_64"

(
    alrb_InstallVersion="9.0.1-x86_64-centos7-gcc8-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.bz2"
    alrb_ToolInstallTag="1607105226"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="10.0.1-x86_64-centos7-gcc8-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.bz2"
    alrb_ToolInstallTag="1607105226"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="11.0.0-x86_64-centos7-gcc8-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.bz2"
    alrb_ToolInstallTag="1607105226"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="13.0.0-x86_64-centos7-gcc11-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="14.0.0-x86_64-centos7-gcc11-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="15.0.4-x86_64-centos7-gcc11-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="16.0.1-x86_64-centos7-gcc11-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="16.0.3-x86_64-centos7-gcc11-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="17.0.6-x86_64-el9-gcc13-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="18.1.3-x86_64-el9-gcc13-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="19.1.0-x86_64-el9-gcc13-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.gz"
    alrb_ToolInstallTag="1727163284"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="19.1.2-x86_64-el9-gcc13-opt"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/clang/clang-${alrb_InstallVersion}.tar.gz"
    alrb_fn_installAction
)


