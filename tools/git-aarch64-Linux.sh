# see masterConfigs.sh for descriptions and defaults of all settings
# put newest versions at the bottom

alrb_InstallDefault="2.41.0-aarch64-Linux"
alrb_InstallTesting="2.41.0-aarch64-Linux"
alrb_InstallPrevious="2.39.0-aarch64-Linux"

alrb_InstallPlatform="aarch64-Linux"

(
    alrb_InstallVersion="2.26.2-aarch64-centos7"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-2.26.2-aarch64-centos7.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.39.0-aarch64-Linux"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

(
    alrb_InstallVersion="2.41.0-aarch64-Linux"
    alrb_InstallTarballDownload="https://atlas-tier3-sw.web.cern.ch/repo/git/git-${alrb_InstallVersion}.tgz"
    alrb_fn_installAction
)

