# These are versions that should be used for sites participating in
#  pre-production release testing
# Send notification of changes to eGroup: atlas-adc-alrb-preproduction@CERN.CH 

\echo "ADCFrontlineSite ALRB settings used ..."

#export ALRB_asetupVersion=testing

#export ALRB_emiVersion=testing

#export ALRB_davixVersion=testing

#export ALRB_rucioVersion=testing

#export ALRB_xrootdVersion=testing

#export ALRB_cvmfsSingularityVersion=testing

#export ALRB_cvmfsApptainerVersion=testing

#export ALRB_logstashVersion=testing

#export ALRB_psutilVersion=testing

#export ALRB_pythonVersion=pilot-testing

#export ALRB_curlVersion=testing
