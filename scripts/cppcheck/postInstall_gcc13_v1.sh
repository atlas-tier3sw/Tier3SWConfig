source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
source $AtlasSetup/scripts/asetup.sh none,gcc13,cmakesetup

\echo " * * * * * "
gcc --version; cmake --version; cmake3 --version
\echo " * * * * * "

\mkdir -p $alrb_cppSrcDir/build
cd $alrb_cppSrcDir/build

cmake -DCMAKE_INSTALL_PREFIX:PATH=$alrb_cppInstallDir/ -DCMAKE_BUILD_TYPE:STRING=Release -DUSE_MATCHCOMPILER:BOOL=ON $alrb_cppSrcDir
if [ $? -ne 0 ]; then
    exit 64
fi

make
if [ $? -ne 0 ]; then
    exit 64
fi

make install
if [ $? -ne 0 ]; then
    exit 64
fi

exit 0


